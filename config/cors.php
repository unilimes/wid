<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Laravel CORS
    |--------------------------------------------------------------------------
    |
    | allowedOrigins, allowedHeaders and allowedMethods can be set to array('*')
    | to accept any value.
    |
    */
   
    'supportsCredentials' => false,
    'allowedOrigins' => ['*'],
    'allowedOriginsPatterns' => [],
    'allowedHeaders' => array('Content-Type', 'Authorization', 'Accept'),
    'allowedMethods' => array('POST', 'PUT', 'GET', 'DELETE'),
    'exposedHeaders' => [],
    'maxAge' => 0,

];
