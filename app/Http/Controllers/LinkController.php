<?php

namespace App\Http\Controllers;

use App\Link;
use Illuminate\Http\Request;

class LinkController extends Controller
{
    public function createLink(Request $request)
    {
        $newLink = new Link();

        $newLink->userID = $request->input('userID');
        $newLink->referenceTo = $request->input('referenceTo');
        $newLink->idReference = $request->input('idReference');

        $newLink->save();

        return response()->json(['success' => true, 'id' => $newLink->id], 200);
    }

    public function getLinks(Request $request)
    {
        $listOfLinks = Link::where('userID', $request['userID'])->get();
        return response()->json($listOfLinks, 200);
    }
}
