<?php

namespace App\Http\Controllers;

use App\ProductAssociation;
use Illuminate\Http\Request;

class ProductAssociationController extends Controller
{
    public static function createProductAssociation($productID, $userEmail) {
        $newProductAssociation = new ProductAssociation();
        $newProductAssociation->productID = $productID;
        $newProductAssociation->createdBy = $userEmail;
        $newProductAssociation->save();
    }

    public function getProductAssociation(Request $request)
    {
        $productAssociation = ProductAssociation::where('productID', $request['productID'])->first();
        return response()->json($productAssociation, 200);
    }

    public function updateProductAssociation(Request $request, $id)
    {
        $productAssociationForUpdate = ProductAssociation::findOrFail($id);
        $productAssociationForUpdate->geometryType = $request->input('geometryType');
        $productAssociationForUpdate->geometryShape = $request->input('geometryShape');
        $productAssociationForUpdate->d1 = $request->input('d1');
        $productAssociationForUpdate->d2 = $request->input('d2');
        $productAssociationForUpdate->d3 = $request->input('d3');
        $productAssociationForUpdate->d4 = $request->input('d4');
        $productAssociationForUpdate->d5 = $request->input('d5');
        $productAssociationForUpdate->textureType = $request->input('textureType');
        $productAssociationForUpdate->needCalculateAzimuth = $request->input('needCalculateAzimuth');
        $productAssociationForUpdate->color = $request->input('color');
        $productAssociationForUpdate->textureUrl = $request->input('textureUrl');
        $productAssociationForUpdate->weightWithoutMountingKit = $request->input('weightWithoutMountingKit');
        $productAssociationForUpdate->weightWithMountingKit = $request->input('weightWithMountingKit');
        $productAssociationForUpdate->modifiedBy = $request->input('modifiedBy');

        $status = $productAssociationForUpdate->save();
        if ($status) { return response()->json(['success' => $status], 200); }
        else { return response()->json(['success' => $status], 403); }
    }
}
