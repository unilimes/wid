<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserDataController extends Controller
{
    public function getUserData(){
        $authUserData = Auth::user();
        $user = User::where('email', $authUserData->email)->first();
        return $user;
    }

    public function updateUserData(Request $request, $id){
        $user = User::findOrFail($id);
        $operationStatus = $user->update($request->all());
        return response()->json($operationStatus, 200);
    }

    public function publicUsers(Request $request){

        $publicUsersArray = array();
        foreach ($request['publicUsersIds'] as $userId) {
            $user = User::findOrFail($userId);
            $publicUsersArray[] = $user;
        }

        return response()->json($publicUsersArray, 200);
    }
}
