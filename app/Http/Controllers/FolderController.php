<?php

namespace App\Http\Controllers;

use App\Folder;
use Illuminate\Http\Request;

class FolderController extends Controller
{
    public function getFolders(Request $request)
    {
        $foldersList = Folder::where('userID', $request->input('userID'))->get();
        return response()->json($foldersList, 200);
    }

    public function createFolder(Request $request)
    {
        $newFolder = new Folder();
        $newFolder->userID = $request->input('userID');
        $newFolder->name = $request->input('name');
        $newFolder->parentID = $request->input('parentID');
        $newFolder->category = $request->input('category');
        $newFolder->notes = $request->input('notes');
        $newFolder->createdBy = $request->input('createdBy');

        $status = $newFolder->save();

        if($status) { return response()->json(['success' => $status, 'id' => $newFolder->id], 200); }
        else { return response()->json(['success' => $status], 403); }
    }

    public function updateFolder(Request $request, $id)
    {
        $folder = Folder::findOrFail($id);
        $folder->userID = $request->input('userID');
        $folder->name = $request->input('name');
        $folder->parentID = $request->input('parentID');
        $folder->category = $request->input('category');
        $folder->notes = $request->input('notes');
        $folder->createdBy = $request->input('createdBy');

        $status = $folder->save();

        if($status) { return response()->json(['success' => $status], 200); }
        else { return response()->json(['success' => $status], 403); }
    }

    public function deleteFolder(Request $request, $id)
    {
        $folder = Folder::findOrFail($id);
        $status = $folder->delete();

        if($status) { return response()->json(['success' => $status], 200); }
        else { return response()->json(['success' => $status], 403); }
    }
}
