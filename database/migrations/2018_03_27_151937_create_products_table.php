<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{

    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->integer('userID')->unsigned();
            $table->integer('parentID')->nullable();
            $table->string('type');
            $table->string('subType');
            $table->string('shape');
            $table->string('manufacture');
            $table->string('model');
            $table->string('notes')->nullable();
            $table->string('mode');
            $table->integer('linkID')->nullable();
            $table->string('createdBy');
            $table->string('modifiedBy')->nullable();
            $table->timestamps();

            $table->foreign('userID')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('products');
    }
}
