<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Folder extends Model
{
    protected $fillable = [
        'userID',
        'name',
        'parentID',
        'category',
        'notes',
        'createdBy',
        'modifiedBy',
    ];
}
