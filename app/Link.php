<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $fillable = [
        'userID',
        'referenceTo',
        'idReference'
    ];
}
