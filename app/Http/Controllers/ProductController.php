<?php

namespace App\Http\Controllers;

use App\Product;
use App\Http\Controllers\ProductAssociationController;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function checkSameEntity($entityList, $entity)
    {
        $check = false;

        foreach ($entityList as $item) {
            $same = true;

            if ($entity->id === $item->id) {
                $same = false;
            }

            if ($entity->type) {
                if ($item->type != $entity->type) {
                    $same = false;
                }
            }

            if ($entity->subType) {
                if ($item->subType != $entity->subType) {
                    $same = false;
                }
            }

            if ($entity->shape) {
                if ($item->shape != $entity->shape) {
                    $same = false;
                }
            }

            if ($entity->manufacture) {
                if ($item->manufacture != $entity->manufacture) {
                    $same = false;
                }
            }

            if ($entity->model) {
                if ($item->model != $entity->model) {
                    $same = false;
                }
            }

            if ($same) {
                $check = true;
            }
        };

        return $check;
    }

    /**
     * @param $entityList
     * @param $ddls
     */
    public function distinctItemsFromEntityList($entityList, &$ddls)
    {
        foreach ($entityList as $entity) {
            if (!in_array($entity->type, $ddls['type'], true)) {
                array_push($ddls['type'], $entity->type);
            }
            if (!in_array($entity->subType, $ddls['subType'], true)) {
                array_push($ddls['subType'], $entity->subType);
            }
            if (!in_array($entity->shape, $ddls['shape'], true)) {
                array_push($ddls['shape'], $entity->shape);
            }
            if (!in_array($entity->manufacture, $ddls['manufacture'], true)) {
                array_push($ddls['manufacture'], $entity->manufacture);
            }
            if (!in_array($entity->model, $ddls['model'], true)) {
                array_push($ddls['model'], $entity->model);
            }
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDDLs(Request $request)
    {
        $ddls = array(
            'type' => [],
            'subType' => [],
            'shape' => [],
            'manufacture' => [],
            'model' => [],
        );

        if (empty($request['pattern'])) {
            $products = Product::whereIn('userID', $request['userIds'])->get();
            $this->distinctItemsFromEntityList($products, $ddls);
            return response()->json($ddls, 200);
        } else {
            $products = Product::whereIn('userID', $request['userIds'])->byPattern($request['pattern'])->get();
            $this->distinctItemsFromEntityList($products, $ddls);
            return response()->json($ddls, 200);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProducts(Request $request)
    {
        $products = Product::where('userID', $request['userID'])->get();
        return response()->json($products, 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProduct(Request $request)
    {
        $product = Product::whereIn('userID', $request['userIds'])->byPattern($request['pattern'])->first();
        return response()->json($product, 200);
    }


    public function createProduct(Request $request)
    {
        $newProduct = new Product();
        $productList = Product::all();

        $newProduct->userID = $request->input('userID');
        $newProduct->name = $request->input('name');
        $newProduct->type = $request->input('type');
        $newProduct->subType = $request->input('subType');
        $newProduct->shape = $request->input('shape');
        $newProduct->manufacture = $request->input('manufacture');
        $newProduct->model = $request->input('model');
        $newProduct->notes = $request->input('notes');
        $newProduct->mode = $request->input('mode');
        $newProduct->createdBy = $request->input('createdBy');

        if(!$this->checkSameEntity($productList, $newProduct)) {
            $newProduct->save();
            ProductAssociationController::createProductAssociation($newProduct->id, $newProduct->createdBy);
        } else {
            return response()->json(['message' => 'Your product must be unique'],400);
        }

        return response()->json(['success' => true, 'id' => $newProduct->id], 200);
    }

    public function updateProduct(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $productList = Product::all();
        if(!$product) return response()->json(['message' => 'This product not found in Database!'], 404);

        $product->type = $request->input('type');
        $product->subType = $request->input('subType');
        $product->shape = $request->input('shape');
        $product->manufacture = $request->input('manufacture');
        $product->model = $request->input('model');
        $product->notes = $request->input('notes');
        $product->parentID = $request->input('parentID');
        $product->modifiedBy = $request->input('modifiedBy');

        if(!$this->checkSameEntity($productList, $product)) {
            $status = $product->save();
        } else {
            return response()->json(['message' => 'Your product must be unique'],400);
        }

        return response()->json($status, 200);
    }

    public function deleteProduct($id)
    {
        $product = Product::find($id);
        $success = $product->delete();
        return response()->json($success, 200);
    }
}
