<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('startPage');
});

Route::get( '/auth0/callback', '\Auth0\Login\Auth0Controller@callback' )->name( 'auth0-callback' );
Route::get('/login', 'Auth\Auth0IndexController@login' )->name( 'login' );
Route::get('/logout', 'Auth\Auth0IndexController@logout' )->name( 'logout' )->middleware('auth');

Route::get('/app', 'Controller@index');

Route::get('/userData/', 'UserDataController@getUserData');
Route::put('/userData/{id}', 'UserDataController@updateUserData');
Route::post('/publicUsers/', 'UserDataController@publicUsers');

// Products
Route::post('/products/ddls', 'ProductController@getDDLs');
Route::post('/products/create', 'ProductController@createProduct');
Route::post('/products/selected', 'ProductController@getProduct');
Route::post('/products', 'ProductController@getProducts');
Route::put('/products/{id}', 'ProductController@updateProduct');
Route::delete('/products/{id}', 'ProductController@deleteProduct');

// Folders
Route::post('/folders', 'FolderController@getFolders');
Route::post('/folders/create', 'FolderController@createFolder');
Route::put('/folders/{id}', 'FolderController@updateFolder');
Route::delete('/folders/{id}', 'FolderController@deleteFolder');

// ProductAssociations
Route::post('/productAssociations', 'ProductAssociationController@getProductAssociation');
Route::post('/productAssociations/create', 'ProductAssociationController@createProductAssociation');
Route::put('/productAssociations/{id}', 'ProductAssociationController@updateProductAssociation');

//Links
Route::post('/links/create', 'LinkController@createLink');
Route::post('/links', 'LinkController@getLinks');
