<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property  productID
 */
class ProductAssociation extends Model
{
    protected $table = 'product_associations';
    protected $fillable = [
        'productID',
        'geometryType',
        'geometryShape',
        'd1',
        'd2',
        'd3',
        'd4',
        'd5',
        'textureType',
        'needCalculateAzimuth',
        'color',
        'textureUrl',
        'weightWithoutMountingKit',
        'weightWithMountingKit',
        'weight1',
        'weight2',
        'weight3',
        'weight4',
        'weight5',
        'ca_f1',
        'ca_f2',
        'ca_f3',
        'ca_f4',
        'ca_f5',
        'ca_s1',
        'ca_s2',
        'ca_s3',
        'ca_s4',
        'ca_s5',
        'ca_s5',
        'mt_pipe_size',
        'mt_pipe_lth',
        'createdBy',
        'modifiedBy',
    ];
}
