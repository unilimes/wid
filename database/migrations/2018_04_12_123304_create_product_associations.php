<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductAssociations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_associations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('productID');
            $table->string('geometryType')->nullable();
            $table->string('geometryShape')->nullable();
            $table->double('d1')->nullable();
            $table->double('d2')->nullable();
            $table->double('d3')->nullable();
            $table->double('d4')->nullable();
            $table->double('d5')->nullable();
            $table->string('textureType')->nullable();
            $table->boolean('needCalculateAzimuth')->nullable();
            $table->string('color')->nullable();
            $table->string('textureUrl')->nullable();
            $table->double('weightWithoutMountingKit')->nullable();
            $table->double('weightWithMountingKit')->nullable();
            $table->double('weight1')->nullable();
            $table->double('weight2')->nullable();
            $table->double('weight3')->nullable();
            $table->double('weight4')->nullable();
            $table->double('weight5')->nullable();
            $table->double('ca_f1')->nullable();
            $table->double('ca_f2')->nullable();
            $table->double('ca_f3')->nullable();
            $table->double('ca_f4')->nullable();
            $table->double('ca_f5')->nullable();
            $table->double('ca_s1')->nullable();
            $table->double('ca_s2')->nullable();
            $table->double('ca_s3')->nullable();
            $table->double('ca_s4')->nullable();
            $table->double('ca_s5')->nullable();
            $table->double('mt_pipe_size')->nullable();
            $table->double('mt_pipe_lth')->nullable();
            $table->string('createdBy');
            $table->string('modifiedBy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_associations');
    }
}
