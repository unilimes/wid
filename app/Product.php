<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed id
 * @property array|string userID
 * @property array|string name
 * @property array|string mode
 * @property array|string type
 * @property array|string subType
 * @property array|string shape
 * @property array|string manufacture
 * @property array|string model
 * @property array|string notes
 * @property array|string createdBy
 * @property array|string checksum
 */
class Product extends Model
{

    public function scopeByPattern($query, $pattern) {
        foreach ($pattern as $patternItem) {
            $query->where($patternItem['descriptor'], $patternItem['value']);
        }
    }

    protected $fillable = [
        'name',
        'userID',
        'parentID',
        'type',
        'subType',
        'shape',
        'manufacture',
        'model',
        'notes',
        'mode',
        'linkID',
        'createdBy',
        'modifiedBy'
    ];

    protected $hidden = [

    ];
}
